#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例040：逆序列表

题目 将一个数组逆序输出。

程序分析 依次交换位置，或者直接调用reverse方法。
"""


def main():
    lis = [1, 10, 100, 1000, 10000, 100000]
    li = []
    for i in range(1, len(lis) + 1):
        li.append(lis[-i])
    print(li)
    lt = sorted(lis, reverse=True)
    print(lt)
    lis.sort(reverse=True)
    print(lis)


if __name__ == "__main__":
    main()
