#!/usr/bin/python3
# -*-coding:utf-8 -*-
"""
题目 输入3个数a,b,c，按大小顺序输出。

程序分析 同实例005。
"""


def main():
    str_input = input('请输入几个数字（用空格隔开)：')
    str_lis = str_input.split(' ')
    int_lis = [int(n) for n in str_lis]
    sort_lis = sorted(int_lis, reverse=True)
    print(sort_lis)


if __name__ == "__main__":
    main()
