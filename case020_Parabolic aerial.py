#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例020：高空抛物

题目 一球从100米高度自由落下，每次落地后反跳回原高度的一半；再落下，求它在第10次落地时，共经过多少米？第10次反弹多高？

程序分析 无
"""


def main():
    total = 0
    higth = 100
    for i in range(10):
        higth = higth / 2
        total += higth + higth * 2
        print(total, higth)
    print('共经过{}米，弹高：{}'.format(total-higth, higth))


if __name__ == "__main__":
    main()
