#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例038：矩阵对角线之和

题目 求一个3*3矩阵主对角线元素之和。

程序分析 无。
"""


def main():
    passmat = [[1, 2, 3], [3, 4, 5], [4, 5, 6]]
    sum = 0
    for i in range(len(passmat)):
        sum += passmat[i][i]
    print(sum)


if __name__ == "__main__":
    main()
