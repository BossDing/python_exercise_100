#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例005：三数排序

题目 输入三个整数x,y,z，请把这三个数由小到大输出。

程序分析 练练手就随便找个排序算法实现一下，偷懒就直接调函数。
"""


def str2int(str_int):
    int_list = []
    str_int_l = str_int.split(' ')
    for i in str_int_l:
        int_list.append(int(i))
    return int_list


def main():
    str_int = input('请输入数字(以空格隔开)：')
    row = str2int(str_int)
    print(row)
    # for i in range(len(row)):
    #     for j in range(i+1, len(row)):
    #         if row[i] > row[j]:
    #             x = row[i]
    #             row[i] = row[j]
    #             row[j] = x
    row.sort(reverse=True)
    print(row)


if __name__ == '__main__':
    main()
