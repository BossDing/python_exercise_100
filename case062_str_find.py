#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例062：查找字符串

题目 查找字符串。

程序分析 无。find() 查找 并返回首位置
"""


def main():
    s1 = 'aabbxuebixuebi'
    s2 = 'ab'
    s3 = 'xue'
    s4 = 'ac'
    i2 = s1.find(s2)
    i3 = s1.find(s3)
    i4 = s1.find(s4)
    if i2 != -1:
        print('\'%s\'在\'%s\'中第%d个开始' % (s2, s1, i2))
    else:
        print('\'%s\'不在\'%s\'中。' % (s2, s1))
    if i3 != -1:
        print('\'%s\'在\'%s\'中第%d个开始' % (s3, s1, i3))
    else:
        print('\'%s\'不在\'%s\'中。' % (s3, s1))
    if i4 != -1:
        print('\'%s\'在\'%s\'中第%d个开始' % (s4, s1, i4))  # 不含有 结果-1
    else:
        print('\'%s\'不在\'%s\'中。' % (s4, s1))


if __name__ == "__main__":
    main()
