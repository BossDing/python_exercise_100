#! /usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例057：画线

题目 画图，学用line画直线。

程序分析 无。
"""
from tkinter import *


def creat_lines():
    global cv
    cv = Canvas(win, width=500, height=500, bg='white')
    cv.pack(expand=YES, fill=BOTH)
    x0 = 263
    y0 = 263
    y1 = 275
    x1 = 275
    global lis
    lis = []
    for i in range(19):
        lis.append(cv.create_line(x0, y0, x0, y1, width=1, fill='red'))
        x0 = x0 - 5
        y0 = y0 - 5
        x1 = x1 + 5
        y1 = y1 + 5
    x0 = 263
    y1 = 275
    y0 = 263
    for i in range(21):
        lis.append(cv.create_line(x0, y0, x0, y1, fill='red'))
        x0 += 5
        y0 += 5
        y1 += 5
    cv.create_text(100, 250, text='王楠')


def delete():
    for i in lis:
        cv.delete(lis[i])
    c = Canvas(win, width=500, height=500, bg='white')
    c.pack()


def main():
    global win
    win = Tk()
    win.title('画直线')
    frame = Frame(win)
    frame.pack()
    but1 = Button(frame, text='王楠', command=creat_lines)
    but1.grid(row=1, column=1)
    but2 = Button(frame, text='删除', command=delete)
    but2.grid(row=2, column=2)
    win.mainloop()


if __name__ == "__main__":
    main()
