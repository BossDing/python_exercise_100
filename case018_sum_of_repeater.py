#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例018：复读机相加

题目 求s=a+aa+aaa+aaaa+aa…a的值，其中a是一个数字。例如2+22+222+2222+22222(此时共有5个数相加)，几个数相加由键盘控制。

程序分析 用字符串解决。
"""


def main():
    a = int(input('请输入一个数字(1-9)：'))
    times = int(input('请输入进行几次相加：'))
    a_list = [a]
    if times >= 2:
        num = int(a)
        for i in range(1, times):
            num = int(a) * 10**i + num
            a_list.append(num)
    print(a_list, end='')
    print(sum(a_list))
    # 字符串直接+ 表示字符串链接
    b = input('被加数字：')
    n = int(input('加几次？：'))
    res = 0
    b_list = []
    for i in range(n):
        b_list.append(int(b))
        res += int(b)
        b += b[0]    # 字符串链接
    print(b_list, '结果是：', res)


if __name__ == "__main__":
    main()
