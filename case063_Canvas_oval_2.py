#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例063：画椭圆

第二版，进行类的封装

题目 画椭圆。

程序分析 使用 tkinter。
"""
from tkinter import *


class CteatOval(object):

    def __init__(self):
        self.root = Tk()
        self.root.title('画椭圆')
        self.root.geometry('600x400')
        frame = Frame(self.root)
        frame.pack()
        self.cv = Canvas(self.root, width='600', height='400', bg='white')
        self.cv.pack()
        creat_button = Button(frame, text='画椭圆', command=self.creatovl)
        delete_button = Button(frame, text='删除', command=self.deletecreat)
        creat_button.grid(row=1, column=2)
        delete_button.grid(row=2, column=2)
        # self.root.mainloop()
        self.x0, self.y0, self.x1, self.y1 = (50, 50, 100, 80)
        self.tags_lis = []

    def creatovl(self):
        tags = self.cv.create_oval(self.x0, self.y0, self.x1, self.y1)
        self.tags_lis.append(tags)
        self.x0 += 50
        self.y0 += 50
        self.x1 += 50
        self.y1 += 50

    def deletecreat(self):
        tags = self.tags_lis[-1]  # 最新绘制的一个标签，删除最后一个
        self.cv.delete(tags)
        self.tags_lis.pop(-1)
        self.x0 -= 50
        self.y0 -= 50
        self.x1 -= 50
        self.y1 -= 50


def main():
    oval = CteatOval()
    oval.root.mainloop()


if __name__ == "__main__":
    main()
