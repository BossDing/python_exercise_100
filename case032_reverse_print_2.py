#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例032：反向输出II

题目 按相反的顺序输出列表的值。

程序分析 无。
"""


def main():
    str_in = input('请输入：')
    print(str_in[::-1])


if __name__ == "__main__":
    main()
