#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例019：完数

题目 一个数如果恰好等于它的因子之和，这个数就称为"完数"。例如6=1＋2＋3.编程找出1000以内的所有完数。

程序分析 将每一对因子加进集合，在这个过程中已经自动去重。最后的结果要求不计算其本身。
"""


def factor(num):
    target = int(num)
    res = set()
    for i in range(1, target):
        if target % i == 0:
            res.add(i)
            res.add(target / i)
    return res


def main():
    for i in range(2, 1001):
        if i == sum(factor(i)) - i:
            print(i)
    wanshu_dict = dict()
    for i in range(2, 10000):
        if i % 1000 == 0:
            print('正在计算。。。')
        li = []
        for j in range(1, i):
            if i % j == 0:
                li.append(j)
        if i == sum(li):
            wanshu_dict[i] = li
            # print(wanshu_dict)
    print('完数：及其因数：')
    for n, nums in wanshu_dict.items():
        print('完数{}：其所有因数：{}'.format(n, nums))


if __name__ == "__main__":
    main()
