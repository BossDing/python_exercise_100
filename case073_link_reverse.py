#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例073：反向输出链表

题目 反向输出一个链表。

程序分析 无。
"""
from case072_link import *


if __name__ == '__main__':
    head = Node('head')
    link = List(head)
    for i in range(10):
        node = Node(i)
        link.append(node)
    print(link.print_list(head))
    print(link.print_list(link.reverse(head)))
