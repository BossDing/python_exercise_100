#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例075：不知所云

题目 放松一下，算一道简单的题目。

程序分析 鬼知道是什么。
"""


def main():
    for i in range(5):
        n = 0
        if i != 1:
            n += 1
        if i == 3:
            n += 1
        if i == 4:
            n += 1
        if i != 4:
            n += 1
        if n == 3:
            print(64 + i)


if __name__ == '__main__':
    main()
