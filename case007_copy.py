#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
题目 将一个列表的数据复制到另一个列表中。

程序分析 使用列表[:]，拿不准可以调用copy模块。
"""
import copy


def main():
    a = [1, 2, 3, 4, ['a', 'b']]

    b = a				    	# 赋值 变量a b 同时指向数列，对数列所有的更改，则a b都会改变
    c = a[:]			    	# 浅拷贝
    d = copy.copy(a)		    # 浅拷贝   浅拷：相当于被list内部的每个元素的指向付给新的变量 相当于 d[0] 和a[0] de 指向是相同的
    e = copy.deepcopy(a)    	# 深拷贝   相当于吧list赋值给e 新建e 指向一个新list 和a是两个不同的list

    a.append(5)
    a[4].append('c')

    print('a=', a, id(a))
    print('b=', b, id(b))
    print('c=', c, id(c), id(c[4]), id(a[4]))
    print('d=', d, id(d), id(d[4]), id(a[4]))
    print('e=', e, id(e), id(e[4]), id(a[4]))


if __name__ == '__main__':
    main()
