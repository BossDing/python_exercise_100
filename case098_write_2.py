#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例098：磁盘写入II

题目 从键盘输入一个字符串，将小写字母全部转换成大写字母，然后输出到一个磁盘文件"test"中保存。

程序分析 无。
"""


def main():
    filename = input('输入文件名:\n')
    with open(filename + '.txt', "a") as fp:
        ch = input('输入字符串:\n')
        ch.upper()
        fp.write('\n'+ch)


if __name__ == '__main__':
    main()
