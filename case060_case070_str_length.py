#! /usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例060：字符串长度

题目 计算字符串长度。

程序分析 无。 len()
"""


def main():
    s = input('请输入:')
    print('\"%s\"的长度是%d' % (s, len(s)))


if __name__ == "__main__":
    main()
