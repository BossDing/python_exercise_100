#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例064：画椭圆、矩形

题目 利用ellipse 和 rectangle 画图。。

程序分析： 采用类实现
"""
from tkinter import *
import random
import tkinter.messagebox


class Creat_OvalandRec(object):
    def __init__(self):
        self.win = Tk()
        self.win.title('画椭圆和矩形')
        self.win.geometry('800x600')
        frame = Frame(self.win)
        frame.pack()
        self.cv = Canvas(self.win, width='800', height='600', bg='white')
        self.cv.pack()
        self.tex = Text(self.win, width='800', height='600')
        self.tex.pack()
        but_creat_ovl = Button(frame, text='画椭圆', comman=self.creat_oval)
        but_creat_rec = Button(frame, text='画矩形', command=self.creat_rect)
        but_del_oval = Button(frame, text='删除椭圆', comman=self.del_oval)
        but_del_rect = Button(frame, text='删除矩形', comman=self.del_rect)
        but_change = Button(frame, text='换位置', comman=self.change)
        but_creat_ovl.grid(row=1, column=1)
        but_creat_rec.grid(row=1, column=2)
        but_del_oval.grid(row=2, column=1)
        but_del_rect.grid(row=2, column=2)
        but_change.grid(row=3, column=1)
        self.x0, self.y0, self.x1, self.y1 = (50, 50, 100, 80)
        self.oval_tags_lis = []
        self.rect_tags_lis = []

    def creat_oval(self):
        tags = self.cv.create_oval(self.x0, self.y0, self.x1, self.y1)
        self.oval_tags_lis.append(tags)

    def creat_rect(self):
        tags = self.cv.create_rectangle(self.x0, self.y0, self.x1, self.y1)
        self.rect_tags_lis.append(tags)

    def del_oval(self):
        try:
            tags = self.oval_tags_lis[-1]
            self.cv.delete(tags)
            self.oval_tags_lis.pop(-1)
        except IndexError:
            # print('已经全部删除')
            tkinter.messagebox.showinfo('错误', '圆\\椭圆已经全部删除！')

    def del_rect(self):
        try:
            tags = self.rect_tags_lis[-1]
            self.cv.delete(tags)
            self.rect_tags_lis.pop(-1)
        except IndexError:
            # print('已经全部删除')
            tkinter.messagebox.showinfo('错误', '矩形已经全部删除！')

    def change(self):
        # x = 10 * random.randint(5, 30)
        # y = 10 * random.randint(5, 30)
        self.x0 = 10 * random.randint(5, 30)
        self.y0 = 10 * random.randint(5, 30)
        self.x1 = 10 * random.randint(5, 30)
        self.y1 = 10 * random.randint(5, 30)


def main():
    cr1 = Creat_OvalandRec()
    cr1.win.mainloop()


if __name__ == "__main__":
    main()
