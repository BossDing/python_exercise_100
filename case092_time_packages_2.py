#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例092：time模块II

题目 时间函数举例2。

程序分析 如何浪费时间。
"""
import time
import functools


def log2(text):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kw):
            start = time.time()
            print('%s开始执行' % text)
            t = func(*args, **kw)
            print('%s执行了%.3fms' % (text, 1000 * (time.time() - start)))
            return t
        return wrapper
    return decorator


def log1(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        start = time.time()
        t = func(*args, **kw)
        print('程序执行了%.3fms' % (1000 * (time.time() - start)))
        return t
    return wrapper


@log2('main()')
def main():
    start = time.time()
    n = 3000
    for i in range(1, n):
        if i % 100 == 0:
            x = 100 * i / n
            print('执行了%.2f %%' % x)
    end = time.time()
    print('共执行了%.3f ms' % ((end - start) * 1000))
    time.sleep(0.1)


@log2('mai()')
def mai(n):
    return n + 1


if __name__ == "__main__":
    main()
    x = 0
    x = mai(x)
    print(x)
