#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例013：所有水仙花数

题目 打印出所有的"水仙花数"，所谓"水仙花数"是指一个三位数，其各位数字立方和等于该数本身。例如：153是一个"水仙花数"，因为153=1的三次方＋5的三次方＋3的三次方。

程序分析 利用for循环控制100-999个数，每个数分解出个位，十位，百位。
"""


def main():
    num_list = []
    for i in range(100, 1000):
        st = str(i)
        x = int(st[0])
        y = int(st[1])
        z = int(st[2])
        # if i == x*x*x + y*y*y + z*z*z:
        if i == x**3 + y**3 + z**3:
            num_list.append(i)
    print(num_list)


if __name__ == '__main__':
    main()
