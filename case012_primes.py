#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例012：100到200的素数
质数又称素数。一个大于1的自然数，除了1和它自身外，不能被其他自然数整除的数叫做质数；否则称为合数。
"""


def num():
    '''
    生成器函数：生成所有的从3开始的奇数
    '''
    n = 1
    while True:
        n += 2
        yield n


def _not_divisible(n):
    """
    筛选函数
    """
    return lambda x: x % n != 0


def primes():
    """
    生成器函数，不断生成下一个素数
    """
    yield 2  # 第一次生成第一个素数2
    it = num()  # it是 生成器对象 初始为 num（）这个生成器，后续根据规则筛选
    while True:
        n = next(it)  # next 下一个 的 第一个值
        # print(n)
        yield n
        it = filter(_not_divisible(n), it)  # 构造新生成序列 赋值于it，即用规则更新后续序列
        # for i in it:
        #     print(i)


def main():
    # 100_200之间的素数
    min_num = int(input('请输入下限值：'))
    max_num = int(input('请输入上限值：'))
    num_list = []
    for i in range(min_num, max_num):
        lis = 0
        for j in range(2, int(i/2)+1):
            if i % j == 0:
                lis = j
                break
        if lis == 0:
            num_list.append(i)
    print(num_list)
    # 所有素数
    max_num = input('求素数工具，退出请输入:q。请输入最大值：')
    num_list = []
    while True:
        if max_num != 'q':
            max_num = int(max_num)
            # print('小于{}的素数如下:'.format(max_num))
            for n in primes():
                if n < max_num:
                    num_list.append(n)
                    # print(n, end='  ')
                else:
                    break
            # print()
            # print('××××××××××××××××××××××××××××××××××××××××××')
            # min_num = int(input('请输入下限值：'))
            # max_num = int(input('请输入上限值：'))
            nums_list = []
            for i in num_list:
                if i in range(min_num, max_num):
                    nums_list.append(i)
            print(nums_list)
            max_num = input('求素数工具，退出请输入:q。请输入最大值：')
        else:
            print('程序退出')
            break


if __name__ == '__main__':
    main()
