#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例027：递归输出

题目 利用递归函数调用方式，将所输入的5个字符，以相反顺序打印出来。

程序分析 递归真是蠢方法。
"""


def string(st):
    if len(st) > 1:
        string(st[1:])
    print(st[0], end=' ')


def main():
    str_in = input('请输入：')
    string(str_in)


if __name__ == "__main__":
    main()
