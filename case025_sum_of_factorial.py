#! /usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例025： 阶乘求和

题目 求1+2!+3!+…+20!的和。

程序分析 1+2!+3!+…+20!=1+2(1+3(1+4(…20(1))))
"""


def main():
    sum_num = 0
    for i in range(1, 21):
        n = 1
        for j in range(1, i + 1):
            n *= j
        sum_num += n
    print(sum_num)


if __name__ == "__main__":
    main()
