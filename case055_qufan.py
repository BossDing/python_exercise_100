#!/usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例055：按位取反

题目 学习使用按位取反~。

程序分析 ~0=1; ~1=0;
"""


def main():
    print(~234)
    print(~~234)


if __name__ == "__main__":
    main()
