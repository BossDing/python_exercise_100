#!/usr/bin/python3
# -*-coding:utf-8 -*-
"""
实例061：杨辉三角

题目 打印出杨辉三角形前十行。

程序分析 无。
"""


def gen_num():
    lis = [1]
    yield lis
    lis = [1, 1]
    yield lis
    while True:
        li = [1, 1]
        for i in range(1, len(lis)):
            li.insert(i, (lis[i - 1] + lis[i]))
        lis = li
        yield lis


def main():
    g = gen_num()
    n = int(input('打印杨辉三角的前几行：'))
    for i in range(n):
        lis = next(g)
        stri = ','.join(str(n) for n in lis)
        print(stri.center(n * 3, ' '))


if __name__ == "__main__":
    main()
