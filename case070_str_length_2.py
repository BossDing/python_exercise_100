#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例070：字符串长度II

题目 写一个函数，求一个字符串的长度，在main函数中输入字符串，并输出其长度。

程序分析 无。
"""


def string_len(s):
    return len(s)


def main():
    stri = input('请输入：')
    print('\"%s\"的长度为:%d' % (stri, string_len(stri)))


if __name__ == "__main__":
    main()
