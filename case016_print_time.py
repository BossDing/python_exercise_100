#!/usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例016：输出日期

题目 输出指定格式的日期。

程序分析 使用 datetime 模块。
"""
import datetime


def main():
    print(datetime.date.today())
    print(datetime.date(2333, 2, 3))
    print(datetime.date.today().strftime('%d/%m/%Y'))
    day = datetime.date(1111, 2, 3)
    day = day.replace(year=day.year + 22)
    print(day)


if __name__ == "__main__":
    main()
