#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例069：报数

题目 有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），凡报到3的人退出圈子，问最后留下的是原来第几号的那位。

程序分析 无。
"""


def dele(lis):
    i = 0  # 控制遍历list
    k = 1  # 控制数数，一直不定的往后数，  不是3的倍数，放到li里面
    while len(lis) >= 2:
        li = []
        while True:
            if k % 3 != 0:
                li.append(lis[i])
            i += 1
            k += 1
            if i == len(lis):
                i = 0
                break
        lis = li
    return lis


def main():
    n = int(input('请输入人数：'))
    li = list(range(1, n + 1))
    lis = dele(li)
    print(lis)

    n = int(input('请输入总人数:'))
    num = []
    for i in range(n):
        num.append(i + 1)

    i = 0
    k = 0
    m = 0

    while m < n - 1:
        if num[i] != 0:
            k += 1
        if k == 3:
            num[i] = 0
            k = 0
            m += 1
        i += 1
        if i == n:
            i = 0

    i = 0
    while num[i] == 0:
        i += 1
    print(num[i])


if __name__ == "__main__":
    main()
