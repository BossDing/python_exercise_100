#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例100：列表转字典

题目 列表转换为字典。

程序分析 zip 函数 将两个list关联起来, zip对象是迭代器
"""
from collections.abc import Iterator


def main():
    a = ['a', 'b']
    b = [1, 2]
    c = zip(a, b)
    print(c)
    print(isinstance(c, Iterator))
    print(list(c))   # c已经迭代完了
    c = zip(a, b)    # 需要重新生成c
    print(dict(c))


if __name__ == '__main__':
    main()
    print(__doc__)
    print(__file__)
