#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例041：类的方法与变量

题目 模仿静态变量的用法。

程序分析 构造类，了解类的方法与变量。
"""


def dummy():
    i = 0
    print(i)
    i += 1


class cls:

    def __init__(self):
        self.i = 0

    def dummy(self):
        print(self.i)
        self.i += 1


def main():
    a = cls()
    for i in range(50):
        dummy()  # 一直打印0
        a.dummy()  # 每次加1


if __name__ == "__main__":
    main()
