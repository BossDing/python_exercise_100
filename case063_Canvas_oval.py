#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例063：画椭圆

题目 画椭圆。

程序分析 使用 tkinter。
"""
from tkinter import *


def main():
    root = Tk()
    root.title('画椭圆')
    root.geometry('600x400')
    frame = Frame(root)
    frame.pack()
    cv = Canvas(root, width='600', height='400', bg='white')
    cv.pack()
    global x0, y0, x1, y1, tags_lis
    x0, y0, x1, y1 = (50, 50, 100, 80)
    tags_lis = []

    def creatovl():
        global x0, y0, x1, y1, tags_lis
        tags_lis.append(cv.create_oval(x0, y0, x1, y1))
        x0 += 50
        y0 += 50
        x1 += 50
        y1 += 50

    def deletecreat():
        global x0, y0, x1, y1, tags_lis
        cv.delete(tags_lis[-1])
        tags_lis.pop(-1)
        x0 -= 50
        y0 -= 50
        x1 -= 50
        y1 -= 50
    creat_button = Button(frame, text='画椭圆', command=creatovl)
    delete_button = Button(frame, text='删除', command=deletecreat)
    creat_button.grid(row=1, column=2)
    delete_button.grid(row=2, column=2)
    root.mainloop()


if __name__ == "__main__":
    main()
