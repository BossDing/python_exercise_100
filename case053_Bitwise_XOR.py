#!/usr/bin/python3
# _*_ coding:utf-8 _*_
"""
实例053：按位异或

题目 学习使用按位异或 ^ 。

程序分析 0^0=0; 0^1=1; 1^0=1; 1^1=0
"""


def main():
    a = 0o77
    print(a ^ 3)
    print(a ^ 3 ^ 7)


if __name__ == "__main__":
    main()
