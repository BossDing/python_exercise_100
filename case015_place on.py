#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例015：分数归档

题目 利用条件运算符的嵌套来完成此题：学习成绩>=90分的同学用A表示，60-89分之间的用B表示，60分以下的用C表示。

程序分析 用条件判断即可。
"""


def get_grade(score):
    if score >= 90:
        grade = 'A'
    elif score < 60:
        grade = 'C'
    else:
        grade = 'B'
    return grade


def main():
    score = int(input('输入分数：'))
    grade = get_grade(score)
    print('分数%d是：%s' % (score, grade))


if __name__ == "__main__":
    main()
