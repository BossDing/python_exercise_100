#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例086：连接字符串II

题目 两个字符串连接程序。

程序分析 无。
"""


def main():
    a = 'guangtou'
    b = 'feipang'
    print(b + a)


if __name__ == "__main__":
    main()
