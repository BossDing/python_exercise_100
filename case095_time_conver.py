#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例095：转换时间格式

题目 字符串日期转换为易读的日期格式。

程序分析 看看就得了，dateutil是个第三方库。
python中时间日期格式化符号：
    %y 两位数的年份表示（00-99）
    %Y 四位数的年份表示（000-9999）
    %m 月份（01-12）
    %d 月内中的一天（0-31）
    %H 24小时制小时数（0-23）
    %I 12小时制小时数（01-12）
    %M 分钟数（00=59）
    %S 秒（00-59）
    %a 本地简化星期名称
    %A 本地完整星期名称
    %b 本地简化的月份名称
    %B 本地完整的月份名称
    %c 本地相应的日期表示和时间表示
    %j 年内的一天（001-366）
    %p 本地A.M.或P.M.的等价符
    %U 一年中的星期数（00-53）星期天为星期的开始
    %w 星期（0-6），星期天为星期的开始
    %W 一年中的星期数（00-53）星期一为星期的开始
    %x 本地相应的日期表示
    %X 本地相应的时间表示
    %Z 当前时区的名称
    %% %号本身

练习：输入日期，判断这一年的第几天，这一年的第几个星期

"""
# from dateutil import parser
# dt = parser.parse("Aug 28 2015 12:00AM")
# print(dt)
import time


def which_day(s):
    tim = time.strptime(s, '%Y-%m-%d')
    day = time.strftime('%j', tim)
    print('%s年中第%s天' % (time.strftime('%Y', tim), time.strftime('%j', tim)))
    return day


def test():
    # tim = time.strptime("Aug 28 2015 12:00AM", '%b %d %Y %H%p')
    tim = time.strptime('Aug 28 2015 12:00AM', '%b %d %Y %H:%M%p')
    print(tim)
    print(time.strftime('%Y-%m-%d %H:%M:%S', tim))
    print('%s年中第%s天' % (time.strftime('%Y', tim), time.strftime('%j', tim)))


def main():
    s = input('请输入日期（YYYY-MM-DD）(退出请输：q):')
    while s not in 'Qq':
        try:
            which_day(s)
        except ValueError:
            print('输入日期各式错误！')
        s = input('请输入日期（YYYY-MM-DD）(退出请输：q):')


if __name__ == "__main__":
    main()
