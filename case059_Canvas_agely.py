#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例059：画图（丑）

题目 画图，综合例子。

程序分析 丑。
"""
from tkinter import *
import math


def main():
    window = Tk()
    window.title('画图，真的很丑。。。')
    window.geometry('300x300')
    cv = Canvas(window, width=300, height=300)
    cv.pack(expand=YES, fill=BOTH)
    x0 = 150
    y0 = 100
    cv.create_oval(x0 - 10, y0 - 10, x0 + 10, y0 + 10)
    cv.create_oval(x0 - 20, y0 - 20, x0 + 20, y0 + 20)
    cv.create_oval(x0 - 50, y0 - 50, x0 + 50, y0 + 50)
    B = 0.809
    for i in range(16):
        a = 2 * math.pi / 16 * i
        x = math.ceil(x0 + 48 * math.cos(a))
        y = math.ceil(y0 + 48 * math.sin(a) * B)
        cv.create_line(x0, y0, x, y, fill='red')
    cv.create_oval(x0 - 60, y0 - 60, x0 + 60, y0 + 60)
    for k in range(501):
        for i in range(17):
            a = (2 * math.pi / 16) * i + (2 * math.pi / 180) * k
            x = math.ceil(x0 + 48 * math.cos(a))
            y = math.ceil(y0 + 48 + math.sin(a) * B)
            cv.create_line(x0, y0, x, y, fill='red')
        for j in range(51):
            a = (2 * math.pi / 16) * i + (2 * math.pi / 180) * k - 1
            x = math.ceil(x0 + 48 * math.cos(a))
            y = math.ceil(y0 + 48 * math.sin(a) * B)
            cv.create_line(x0, y0, x, y, fill='red')
    window.mainloop()


if __name__ == "__main__":
    main()
