#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例071：输入和输出

题目 编写input()和output()函数输入，输出5个学生的数据记录。

程序分析 无。
"""


def student(n):
    student = []
    for i in range(n):
        student.append(['', '', []])
    return student


def input_stu(stu):
    for i in range(len(stu)):
        stu[i][0] = input('input student num:\n')
        stu[i][1] = input('input student name:\n')
        for j in range(3):
            stu[i][2].append(int(input('score:\n')))


def output_stu(stu):
    for i in range(len(stu)):
        print('%-6s%-10s' % (stu[i][0], stu[i][1]))
        for j in range(3):
            print('%-8d' % stu[i][2][j])


if __name__ == '__main__':
    n = int(input('请输入学生个数：'))
    stu_lis = student(n)
    print('请输入每个学生的num，name，和三个score')
    input_stu(stu_lis)
    print(student)
    output_stu(stu_lis)
