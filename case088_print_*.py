#!/usr/bin/python3
# -*coding:utf-8 -*-
"""
实例088：打印星号

题目 读取7个数（1—50）的整数值，每读取一个值，程序打印出该值个数的＊。

程序分析 无。
"""


def print_x(n):
    print('*' * n)


def main():
    n = input('请输入一个数(Q退出)：')
    while n not in 'qQ':
        print_x(int(n))
        n = input('请输入一个数(Q退出)：')


if __name__ == "__main__":
    main()
