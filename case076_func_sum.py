#!/usr/bin/python3
# -*- coding:utf-8 -*-
"""
实例076：做函数

题目 编写一个函数，输入n为偶数时，调用函数求1/2+1/4+…+1/n,当输入n为奇数时，调用函数1/1+1/3+…+1/n

程序分析 无。
"""


def sum(n):
    if n % 2 != 0:
        print('%d是奇数' % n)
        s = 0
        for i in range(1, n+1, 2):  # range()第三个参数表示步长
            s += 1 / i
        return s
    elif n % 2 == 0:
        print('%d是偶数' % n)
        s = 0
        for i in range(2, n+1, 2):
            s += 1 / i
        return s
    else:
        print('无法计算！')


def main():
    n = int(input('请输入一个数：'))
    su = sum(n)
    print(su)


if __name__ == "__main__":
    main()
