#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例004：这天第几天

题目 输入某年某月某日，判断这一天是这一年的第几天？

程序分析 特殊情况，闰年时需考虑二月多加一天
"""


def is_leapyear(y):
    leapyear = False
    if y % 400 == 0 or (y % 4 == 0 and y % 100 != 0):
        leapyear = True
    return leapyear


def get_Y_M_D(s):
    s_list = s.split('/')
    year = int(s_list[0])
    month = int(s_list[1])
    day = int(s_list[2])
    return year, month, day


def main():
    DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    num_days = 0
    str_day = input('请输入当前时间(YYYY/MM/DD)：')
    year, month, day = get_Y_M_D(str_day)
    for i in range(1, month):
        num_days += DAYS[i-1]
    num_days += day
    if is_leapyear(year):
        print('%d年是润年' % year)
        if month > 2:
            num_days += 1
    else:
        print('%d年不是润年' % year)
    print('%s 是这一年的第%d天' % (str_day, num_days))


if __name__ == '__main__':
    main()
