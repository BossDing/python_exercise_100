#! /usr/bin/python3
# _*_ coding:UTF-8 _*_
"""
实例001：数字组合

题目 有四个数字：1、2、3、4，能组成多少个互不相同且无重复数字的三位数？各是多少？

程序分析 遍历全部可能，把有重复的剃掉。
"""
import itertools


def main_for():
    total = []
    for i in range(1, 5):
        for j in range(1, 5):
            for k in range(1, 5):
                if i != j and j != k and i != k:
                    total.append(i*100+j*10+k)
    print('共有数字{}个:\n'.format(len(total)), total)


def main_permutations():
    total = []
    a = range(1, 5)
    for i in itertools.permutations(a, 3):
        total.append(i[0]*100+i[1]*10+i[2])
    print('共有数字{}个:\n'.format(len(total)), total)


if __name__ == '__main__':
    main_for()
    main_permutations()
